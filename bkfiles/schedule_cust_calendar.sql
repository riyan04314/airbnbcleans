-- phpMiniAdmin dump 1.9.170730
-- Datetime: 2020-01-08 07:39:51
-- Host: 
-- Database: houseclean

/*!40030 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

DROP TABLE IF EXISTS `schedule_cust_calendar`;
CREATE TABLE `schedule_cust_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `cleaner_id` int(11) NOT NULL,
  `dtd` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `rating_msg` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `schedule_cust_calendar` DISABLE KEYS */;
INSERT INTO `schedule_cust_calendar` VALUES ('1','2','26','16','1','21','2019-12-31','11:00:00','12:30:00',NULL,'2020-01-06 18:36:31','4','Good Job'),('2','2','26','16','1','21','2020-01-01','09:00:00','10:30:00',NULL,'2020-01-07 17:26:04','4','World Best Cleaner'),('3','2','26','16','1','23','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:24:47','0',NULL),('4','2','26','16','1','22','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:25:28','5','Good Job'),('5','2','26','16','1','27','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:24:57','0',NULL),('6','1','34','24','1','22','2020-01-05','09:00:00','10:30:00',NULL,NULL,'0',NULL),('7','2','26','16','1','25','2020-01-01','09:00:00','10:30:00',NULL,'2020-01-07 17:27:04','0',NULL),('8','2','26','16','1','25','2020-01-04','09:00:00','10:30:00',NULL,'2020-01-07 17:26:41','0',NULL),('9','2','26','16','1','25','2020-01-05','09:00:00','10:30:00',NULL,'2020-01-07 17:26:49','0',NULL),('10','2','26','16','1','25','2019-12-03','09:00:00','10:30:00',NULL,'2020-01-07 17:27:53','4','World Cleaner'),('11','2','26','16','1','25','2020-01-06','15:00:00','16:30:00',NULL,'2020-01-07 17:28:51','4','Better Cleaner'),('12','4','26','16','1','25','2020-01-15','09:00:00','10:30:00',NULL,'2020-01-07 17:24:19','0',NULL),('13','1','26','16','1','25','2020-01-29','09:00:00','10:30:00',NULL,NULL,'0',NULL),('14','1','26','16','1','25','2020-02-05','09:00:00','10:30:00',NULL,NULL,'0',NULL),('15','1','26','16','1','25','2020-01-16','09:00:00','10:30:00',NULL,NULL,'0',NULL);
/*!40000 ALTER TABLE `schedule_cust_calendar` ENABLE KEYS */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;


-- phpMiniAdmin dump end
