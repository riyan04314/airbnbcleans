-- phpMiniAdmin dump 1.9.170730
-- Datetime: 2020-01-08 07:40:49
-- Host: 
-- Database: houseclean

/*!40030 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  `address_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`),
  CONSTRAINT `address_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES ('11','21','1','London',NULL,NULL),('12','22','1','New Jersey, New Old Road',NULL,NULL),('13','23','3','WDC City',NULL,NULL),('14','24','1','New Jersey, New Old Road',NULL,NULL),('15','25','1','New Jersey, New Old Road',NULL,NULL),('16','26','1','23 New Jersey',NULL,NULL),('17','27','3','New Jersey, New Old Road',NULL,NULL),('18','28','4','New Jersey, New Old Road',NULL,NULL),('19','29','1','New Jersey, New Old Road',NULL,NULL),('20','30','2','123 Main Road.',NULL,NULL),('21','31','1','New Jersey, New Old Road',NULL,NULL),('22','32','2','New Jersey, New Old Road',NULL,NULL),('23','33','1','cvalaxcvcxv',NULL,NULL),('24','34','1','123 Main Road',NULL,NULL),('25','35','1','23 New Jersey',NULL,NULL);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;

DROP TABLE IF EXISTS `cleaner_calendar`;
CREATE TABLE `cleaner_calendar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cleaner_id` int(11) NOT NULL,
  `dtd` date DEFAULT NULL,
  `start_time` int(11) NOT NULL DEFAULT '900',
  `end_time` int(11) NOT NULL DEFAULT '1800',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `cleaner_calendar` DISABLE KEYS */;
INSERT INTO `cleaner_calendar` VALUES ('1','23','2020-01-25','7','15','0',NULL,NULL),('2','23','2020-02-05','5','19','0',NULL,NULL),('3','23','2020-01-18','7','9','0',NULL,NULL),('4','23','2020-01-18','10','12','0',NULL,NULL),('5','23','2020-01-18','13','16','0',NULL,NULL),('6','23','2020-01-18','17','20','0',NULL,NULL),('7','23','2020-01-19','7','11','0',NULL,NULL),('8','23','2020-01-19','12','15','0',NULL,NULL),('9','23','2020-01-19','15','18','0',NULL,NULL),('10','23','2020-01-23','5','18','0',NULL,NULL),('11','25','2020-01-25','7','15','0',NULL,NULL),('12','25','2020-02-05','5','19','0',NULL,NULL),('13','25','2020-01-16','7','9','0',NULL,NULL),('14','25','2020-01-16','10','12','0',NULL,NULL),('15','25','2020-01-16','13','16','0',NULL,NULL),('16','25','2020-01-16','17','20','0',NULL,NULL),('17','25','2020-01-19','7','11','0',NULL,NULL),('18','25','2020-01-19','12','15','0',NULL,NULL),('19','25','2020-01-19','15','18','0',NULL,NULL),('20','25','2020-01-23','5','18','0',NULL,NULL),('21','27','2020-01-25','7','15','0',NULL,NULL),('22','27','2020-02-05','5','19','0',NULL,NULL),('23','27','2020-01-18','7','9','0',NULL,NULL),('24','27','2020-01-18','10','12','0',NULL,NULL),('25','27','2020-01-18','13','16','0',NULL,NULL),('26','27','2020-01-18','17','20','0',NULL,NULL),('27','27','2020-01-19','7','11','0',NULL,NULL),('28','27','2020-01-19','12','15','0',NULL,NULL),('29','27','2020-01-19','15','18','0',NULL,NULL),('30','27','2020-01-23','5','18','0',NULL,NULL);
/*!40000 ALTER TABLE `cleaner_calendar` ENABLE KEYS */;

DROP TABLE IF EXISTS `cleaner_calendar_slot`;
CREATE TABLE `cleaner_calendar_slot` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cleaner_id` int(11) NOT NULL,
  `dtd` date DEFAULT NULL,
  `slot` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `cleaner_calendar_slot` DISABLE KEYS */;
/*!40000 ALTER TABLE `cleaner_calendar_slot` ENABLE KEYS */;

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES ('1','NewYork','NewYork CityTest','2019-12-15 03:14:10','2019-12-28 14:20:43'),('2','New Jersey','Test','2019-12-15 03:14:26','2019-12-15 03:14:26'),('3','Washington D.C.','Test City','2019-12-16 18:01:32','2019-12-16 18:01:32'),('4','Dallas Texas','Dallas Texas City','2019-12-28 14:19:09','2019-12-28 14:19:09'),('5','Los Angeles','Los Angeles	City','2019-12-28 14:22:07','2019-12-28 14:22:07'),('6','Houston','Houston	City','2019-12-28 14:22:37','2019-12-28 14:26:03'),('7','Chicago','Chicago	City','2019-12-28 14:24:42','2019-12-28 14:24:42');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('1','2014_10_12_000000_create_users_table','1'),('2','2014_10_12_100000_create_password_resets_table','1'),('3','2019_08_19_000000_create_failed_jobs_table','1'),('4','2019_10_23_011513_create_products_table','1'),('5','2019_05_03_000001_create_customer_columns','2'),('6','2019_05_03_000002_create_subscriptions_table','2'),('7','2019_12_29_144011_create_plans_table','3');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

DROP TABLE IF EXISTS `plans`;
CREATE TABLE `plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plans_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `plans` DISABLE KEYS */;
INSERT INTO `plans` VALUES ('1','Express Clean','express','prod_GRnhJYyWooCwjE','10.00',NULL,NULL,NULL),('2','Combo Clean','combo','prod_GRnhJYyWooCwjE','50.00',NULL,NULL,NULL);
/*!40000 ALTER TABLE `plans` ENABLE KEYS */;

DROP TABLE IF EXISTS `schedule_calendar`;
CREATE TABLE `schedule_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slot_start_time` datetime NOT NULL,
  `slot_end_time` datetime NOT NULL,
  `cleaner_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Express, 2: Combo',
  `available_cleaner` int(11) NOT NULL DEFAULT '0',
  `booked_cleaner` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `schedule_calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_calendar` ENABLE KEYS */;

DROP TABLE IF EXISTS `schedule_cleaner_calendar`;
CREATE TABLE `schedule_cleaner_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `cleaner_id` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `slot_start_time` datetime NOT NULL,
  `slot_end_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `schedule_cleaner_calendar` DISABLE KEYS */;
INSERT INTO `schedule_cleaner_calendar` VALUES ('1','1','23','1','2019-12-15 10:00:00','2019-12-15 18:00:00',NULL,NULL),('4','1','23','1','2019-12-17 11:00:00','2019-12-17 16:30:00',NULL,NULL),('6','1','23','1','2019-12-19 11:00:00','2019-12-19 16:30:00',NULL,NULL),('7','1','23','1','2019-12-21 11:00:00','2019-12-21 16:30:00',NULL,NULL),('8','0','22','1','2019-12-15 12:00:00','2019-12-15 17:00:00',NULL,NULL),('9','0','22','1','2019-12-17 10:00:00','2019-12-17 15:00:00',NULL,NULL),('11','0','22','1','2019-12-19 10:00:00','2019-12-19 17:30:00',NULL,NULL),('12','0','22','1','2019-12-20 11:00:00','2019-12-20 18:00:00',NULL,NULL),('13','0','21','1','2019-12-21 10:30:00','2019-12-21 18:00:00',NULL,NULL),('14','0','21','1','2019-12-16 13:00:00','2019-12-16 18:00:00',NULL,NULL),('15','0','21','1','2019-12-22 13:30:00','2019-12-22 16:00:00',NULL,NULL),('16','1','21','1','2019-12-23 14:30:00','2019-12-23 18:30:00',NULL,NULL),('17','3','23','1','2019-12-29 11:00:00','2019-12-29 16:00:00',NULL,NULL);
/*!40000 ALTER TABLE `schedule_cleaner_calendar` ENABLE KEYS */;

DROP TABLE IF EXISTS `schedule_cust_calendar`;
CREATE TABLE `schedule_cust_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `customer_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `cleaner_id` int(11) NOT NULL,
  `dtd` date NOT NULL,
  `from_time` time NOT NULL,
  `to_time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `rating_msg` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*!40000 ALTER TABLE `schedule_cust_calendar` DISABLE KEYS */;
INSERT INTO `schedule_cust_calendar` VALUES ('1','2','26','16','1','21','2019-12-31','11:00:00','12:30:00',NULL,'2020-01-06 18:36:31','4','Good Job'),('2','2','26','16','1','21','2020-01-01','09:00:00','10:30:00',NULL,'2020-01-07 17:26:04','4','World Best Cleaner'),('3','2','26','16','1','23','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:24:47','0',NULL),('4','2','26','16','1','22','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:25:28','5','Good Job'),('5','2','26','16','1','27','2020-01-02','09:00:00','10:30:00',NULL,'2020-01-07 17:24:57','0',NULL),('6','1','34','24','1','22','2020-01-05','09:00:00','10:30:00',NULL,NULL,'0',NULL),('7','2','26','16','1','25','2020-01-01','09:00:00','10:30:00',NULL,'2020-01-07 17:27:04','0',NULL),('8','2','26','16','1','25','2020-01-04','09:00:00','10:30:00',NULL,'2020-01-07 17:26:41','0',NULL),('9','2','26','16','1','25','2020-01-05','09:00:00','10:30:00',NULL,'2020-01-07 17:26:49','0',NULL),('10','2','26','16','1','25','2019-12-03','09:00:00','10:30:00',NULL,'2020-01-07 17:27:53','4','World Cleaner'),('11','2','26','16','1','25','2020-01-06','15:00:00','16:30:00',NULL,'2020-01-07 17:28:51','4','Better Cleaner'),('12','4','26','16','1','25','2020-01-15','09:00:00','10:30:00',NULL,'2020-01-07 17:24:19','0',NULL),('13','1','26','16','1','25','2020-01-29','09:00:00','10:30:00',NULL,NULL,'0',NULL),('14','1','26','16','1','25','2020-02-05','09:00:00','10:30:00',NULL,NULL,'0',NULL),('15','1','26','16','1','25','2020-01-16','09:00:00','10:30:00',NULL,NULL,'0',NULL);
/*!40000 ALTER TABLE `schedule_cust_calendar` ENABLE KEYS */;

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscriptions_user_id_stripe_status_index` (`user_id`,`stripe_status`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `subscriptions` DISABLE KEYS */;
INSERT INTO `subscriptions` VALUES ('9','34','upticks.io@gmail.com','sub_GTTWmxf16qoYwp','PaymentDone','plan_GSQB4nqOJPNamT','10','2020-05-01 00:00:00','2020-05-01 00:00:00','2020-01-02 16:30:34','2020-01-02 16:30:35'),('10','34','upticks.io@gmail.com','sub_GTTf96LoDuqbim','PaymentDone','plan_GSQCkEc344j1JU','5','2020-02-01 00:00:00','2020-02-01 00:00:00','2020-01-02 16:40:04','2020-01-02 16:40:04'),('11','35','upticks.io@gmail.com','sub_GV3S7nkjESaYrR','PaymentDone','plan_GSQCkEc344j1JU','1','2020-02-05 00:00:00','2020-02-05 00:00:00','2020-01-06 21:42:46','2020-01-06 21:48:51'),('12','35','upticks.io@gmail.com','sub_GV3YJPHsN49ASW','PaymentDone','plan_GSQCkEc344j1JU','1','2020-02-05 00:00:00','2020-02-05 00:00:00','2020-01-06 21:48:50','2020-01-06 21:48:51');
/*!40000 ALTER TABLE `subscriptions` ENABLE KEYS */;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(4) NOT NULL DEFAULT '1' COMMENT '1:General User, 2: Admin, 3: Cleaner',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'profile.png',
  `govtid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'govtid.jpg',
  `gender` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:Male, 2:Female, 3:Other',
  `dob` date DEFAULT NULL,
  `rating` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_review` int(11) NOT NULL DEFAULT '0',
  `task_complete` int(11) NOT NULL DEFAULT '0',
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_stripe_id_index` (`stripe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','2','Admin Jhon Cronie','support@69ideas.com',NULL,'$2y$10$sWxi0a.RHXpKSiMjvD5fsOaxxhfEglbISZHV3EK44WLF7XWu.y9ly',NULL,NULL,'1','2019-12-15 02:12:36','2019-12-15 02:31:40',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('2','1','Jhon Cronie','724test02121@gmail.com',NULL,'$2y$10$EayYei1zRJsOUY5DaNkV/exd7YUgeQVdcb8a3kbGZgfYTLX0eml4S',NULL,NULL,'1','2019-12-15 03:25:23','2019-12-15 03:25:23',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('3','1','Jhon Cronie','9064test02121@gmail.com',NULL,'$2y$10$iyBNYQnSCIwmxFtp45No4ucUB7D20G6M3K.dcgyI3.Z1iZX3WQjSa',NULL,NULL,'1','2019-12-15 03:25:50','2019-12-15 03:25:50',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('19','1','Jhon','2445upticks.io@gmail.com',NULL,'$2y$10$Qp9lrmROyMFrOlk9HEnqXuMvAXAumFWfg3swegADd5MIA.ORQsw2m',NULL,'12345645','1','2019-12-15 12:22:00','2019-12-15 12:22:00',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('21','3','Jhon','7217upticks.io@gmail.com',NULL,'$2y$10$.fKwzjqXu/6sCQySgx1V8u3jHmyKCz9QWEOJh8yIXOqc9jTGznPrS',NULL,'12345645','1','2019-12-15 12:23:32','2020-01-07 17:26:04',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'4.00','2','2',NULL,NULL),('22','3','Tester','tester05@gmail.com',NULL,'$2y$10$McBl/j4fP2ZY2lf5DAdhLOWJ437KxKcWFVqPRtKsjjwuuGJg77sZ2',NULL,'12345678','1','2019-12-16 18:39:52','2020-01-07 17:25:28',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'5.00','1','1',NULL,NULL),('23','3','Jhon','test05@gmail.com',NULL,'$2y$10$kSkF6UY9bJDI6pwFHIG3auYXoeBm.i6avRqfLTeOpkDbyp1pKlGlS',NULL,'12345678','1','2019-12-16 18:44:08','2019-12-16 18:44:08',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('24','3','Test Cleaner','testcleaner01@gmail.com',NULL,'$2y$10$LizhMaOqYtusrgY/oXbpJegSoxHCCUAC9iyVNBu2weFAug54f7Rx6',NULL,'1234564561','1','2019-12-17 01:57:23','2019-12-17 01:57:23',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('25','3','Jhon 04','cleaner04@gmail.com',NULL,'$2y$10$DvkZt0ulLSbTdMX5SAFFMeZuj523K2b28rtR815qae97eUPSVGb1C',NULL,'12345678','1','2019-12-17 02:32:45','2020-01-07 17:28:51',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'4.00','2','5',NULL,NULL),('26','1','Rofik','customer01@gmail.com',NULL,'$2y$10$gsDYH.Om.eZhNjZjPpK5MOpFyzIHDVoydlLDMR3E8A9t0KudAqys6',NULL,'1231231231','1','2019-12-25 16:08:14','2019-12-29 08:03:42','cus_GRqRkuvMmAHzvm',NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('27','3','Cleaner 06','cleaner06@gmail.com',NULL,'$2y$10$3xkgg5A4roRzPYdZ4ov8RekN7k1KEgNYyX4ALqgDfEFAKDE3WZkXG',NULL,'1231231234','1','2019-12-25 19:45:04','2019-12-25 19:45:04',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('28','1','Riyan Cronie','test012@gmail.com',NULL,'$2y$10$8l8pPPx9y1pdCuUxsDPR6OH/5def3g/3lEzjN50fAhwEbqHYBQcby',NULL,'1254125412','0','2019-12-29 17:38:43','2019-12-29 17:38:43',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('29','1','Riyan Cronie','test014@gmail.com',NULL,'$2y$10$cSxNP5BWINFtfImbvW.cW.EsqA2Usep5AoIqVp6ym/s.B3GErw//O',NULL,'1254125412','0','2019-12-29 17:40:23','2019-12-29 17:40:23',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('30','1','Customer 02','customer02@gmail.com',NULL,'$2y$10$SjTX7iYjl0b3Mqs6VxHIe.Ccyo9hYEKn23JPHMfmi/IQ/AH0u4Q1W',NULL,'1234567891','0','2019-12-30 15:34:42','2019-12-30 15:34:42',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('31','1','Test Cronie','trial@gmail.com',NULL,'$2y$10$AKg.1fpboswCcUabXMrca.cklmgruLyuw.CtStjEyeMT5YocBrMpK',NULL,'12546321','0','2019-12-30 22:16:10','2019-12-30 22:16:10',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('32','1','tester7898','tester7898e@gmail.com',NULL,'$2y$10$lklII2BwxeJ803spsS/Eg.0yi.0PEw51p0NeO3OcKtDDgvC0vZKri',NULL,'12345612','0','2020-01-01 15:10:23','2020-01-01 15:10:23',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('33','1','Test325','test325e@gmail.com',NULL,'$2y$10$Wc717kBoXa/7eyy.MPmMR.TRZFiqxy5icRIOhoEJYOM96EN9RK8De',NULL,'1145211','0','2020-01-01 15:14:49','2020-01-01 15:14:49',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('34','1','Tester','tester45@gmail.com',NULL,'$2y$10$PawKDOulpueS.ZjdnLXqxuhdarj79M34taksawJU6FO8qS3zcJCn.',NULL,'1234567894','0','2020-01-02 16:30:35','2020-01-02 16:30:35',NULL,NULL,NULL,NULL,'profile.png',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('35','1','Testing','test09@gmail.com',NULL,'$2y$10$.IDl.GCBT6YU9nNkho/GIugJaq6JrHJclpIb.DhnyLUX0AADIQY6W',NULL,'24233535','0','2020-01-06 21:48:51','2020-01-06 21:51:27',NULL,NULL,NULL,NULL,'5541578347487.jpg',NULL,'1',NULL,'0.00','0','0',NULL,NULL),('39','3','Riyan Bemachadan','riyan04314@gmail.com',NULL,NULL,NULL,NULL,'0','2020-01-07 06:35:34','2020-01-07 06:35:34',NULL,NULL,NULL,NULL,'profile.png','govtid.jpg','1',NULL,'0.00','0','0','109767939518472727615',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;


-- phpMiniAdmin dump end
