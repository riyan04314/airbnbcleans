<?php

############################################################################
#    Copyright (C) 2009 by Geo Varghese : http://www.seofreetools.net/     #
#    sendtogeo@gmail.com                                                   #
#                                                                          #
#    This program is free software; you can redistribute it and#or modify  #
#    it under the terms of the GNU General Public License as published by  #
#    the Free Software Foundation; either version 2 of the License, or     #
#    (at your option) any later version.                                   #
#                                                                          #
#    This program is distributed in the hope that it will be useful,       #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#    GNU General Public License for more details.                          #
#                                                                          #
#    You should have received a copy of the GNU General Public License     #
#    along with this program; if not, write to the                         #
#    Free Software Foundation, Inc.,                                       #
#    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             #
############################################################################


class Calendar{
    var $currentMonth;
    var $currentYear;
    var $currentDate;
    var $currentTime;
    var $prevTime;
    var $nextTime;
    var $weekDays;

    # func to initialise calendar
    function LoadCalendar($timeStamp){
        if(!empty($timeStamp)){
            $this->currentYear =  date('Y',$timeStamp);
            $this->currentMonth = date('m',$timeStamp); 
        }else{
            $this->currentYear =  date('Y');
            $this->currentMonth = date('m');
        }
        $this->currentDate = date('d');
        $this->currentTime = mktime(0, 0, 0, $this->currentMonth , 1 , $this->currentYear);
        $prevMonth = $this->currentMonth - 1;
        $nextMonth = $this->currentMonth + 1;
        $this->prevTime = mktime(0, 0, 0, $prevMonth , 1 , $this->currentYear);
        $this->nextTime = mktime(0, 0, 0, $nextMonth , 1 , $this->currentYear);
        $this->weekDays = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    }

    # func to display calendar
    function DisplayCalendar($timeStamp=""){
        $this->LoadCalendar($timeStamp);

        $currentTime = mktime(0, 0, 0, $this->currentMonth , 1 , $this->currentYear);
        $prevMonth = $this->currentMonth - 1;
        $nextMonth = $this->currentMonth + 1;
        $prevTime = mktime(0, 0, 0, $prevMonth , 1 , $this->currentYear);
        $nextTime = mktime(0, 0, 0, $nextMonth , 1 , $this->currentYear);

		$monthNumber	= date("m",$currentTime);
        $yearNumber		= date("Y",$currentTime);
        $monthText 	= date("F",$currentTime);
        $yearText 	= date("Y",$currentTime);
        $dayText 	= date("d",$currentTime);

        $totalDays = date('t', $currentTime);
        $firstDay = date("D", $currentTime);
        $weekDays = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
        $listMonth = array();
        $prevMonthText = date('F', $prevTime);        
        $nextMonthText = date('F', $nextTime);

        $today = mktime();

        # to find the day of 1st day in a month
        $index = array_search($firstDay, $weekDays);

        # starts to print calender
        echo '
<div class="month currentMonth">      
	<ul>
		<li class="prev" onclick="mycalendar('.$prevTime.')" style="cursor:pointer; '.((strtotime('01-'.$monthNumber.'-'.$yearNumber)<time())?'display:none;':'').'">&#10094;</li>
		<li class="next" onclick="mycalendar('.$nextTime.')" style="cursor:pointer;">&#10095;</li>
		<li>'.$monthText.'<br>
			<span style="font-size:18px">'.$yearText.'</span>
		</li>
	</ul>
</div>        
<ul class="weekdays currentMonth" style="margin-top:0px;">
	<li>Su</li>
	<li>Mo</li>
	<li>Tu</li>
	<li>We</li>
	<li>Th</li>
	<li>Fr</li>
	<li>Sa</li>
</ul>
<ul class="days currentMonth">';
        $w = 0;
        for($m = 1; $m <= $totalDays ; ){
            $d = ($m == 1) ? $index : 0;
            for( ; $d <= 6 ; $d++){
                $listMonth[$w][$d] = $m;
                $m++;
                if($m > $totalDays){break;}
            }
            $w++;
        }

		
		$day = 1;
        # to get total details of a month
        foreach($listMonth as $styleIdx => $listWeek){
            
            for($i = 0 ; $i <= 6 ; $i++){
                if (isset($listWeek[$i])) {
                    $day = $listWeek[$i] < 10 ? "0".$listWeek[$i] : $listWeek[$i];
                    $month = $this->currentMonth;
                    $content = $day;
                } else {
                    $content = "&nbsp;";
                }
                $style = ($styleIdx % 2) ? "background-color:#E6E6E6" : "";
                if(isset($listWeek[$i]) && $listWeek[$i] == date('d')){ 
                    $style = "background-color:#868686"; 
                }
				//echo '<li><span style="color:#000;">'.$content.'</span></li>';
				
				if(strtotime($day.'-'.$monthNumber.'-'.$yearNumber) < time()){
					echo '<li style="color:#000;" >'.$content.'</li>';
				}
				elseif($day.'-'.$monthNumber.'-'.$yearNumber == date('d-m-Y')){
					echo '<li style="background-color:#1abc9c;" ><a class="active" onclick="setavailabletime(\''.$yearNumber.'-'.$monthNumber.'-'.$day.'\')" href="#availableCalenderModal" rel="modal:open">'.$content.'</a></li>';
				}
				else{									
					echo '<li><a style="color:#CCC;" onclick="setavailabletime(\''.$yearNumber.'-'.$monthNumber.'-'.$day.'\')"  href="#availableCalenderModal" rel="modal:open">'.$content.'</a></li>';
				}
			}
            
        }
        echo '</ul>';
            
    }
}

$t = time();
if(isset($_GET['timeStamp'])){
	$t = $_GET['timeStamp'];
}


$calendar = New Calendar();
$calendar->DisplayCalendar($t);

?>