<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/cleaner/register', function () {
    return view('auth/cleanerregister');
});
Route::get('/', function () {
    return view('welcome');
});
Route::any('sender', function () {
    $txt = request()->txt;
	echo $txt;
	if(isset($txt) && $txt != '')
	event(new \App\Events\FormSubmitted($txt));
	return view('sender');
});

Route::get('payment', 'PayPalController@payment')->name('payment');
Route::get('cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');
Route::get('/stripe', 'MyStripeController@index');
Route::post('/store', 'MyStripeController@store')->name('stripe.store');	
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dbupdate', 'Controller@dbupdate')->name('dbupdate');
Route::any('/getLocation', 'Controller@getLocation')->name('getLocation');

Route::group(['middleware' => 'auth','before'=>'auth','after'=>'no-cache'], function() {
	Route::post('/bookcleaner/{id}', 'HomeController@bookcleaner')->name('bookcleaner');
	Route::any('/searchcleaner', 'HomeController@searchcleaner')->name('searchcleaner');

	
    Route::get('/plans', 'PlanController@index')->name('plans.index');
    Route::get('/plan/{plan}', 'PlanController@show')->name('plans.show');
	Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');
	Route::post('/addreview', 'HomeController@addreview')->name('addreview');
	Route::any('/cancelschedule/{id}', 'HomeController@cancelschedule')->name('cancelschedule');
	//Route::get('orderlist', 'HomeController@orderlist')->name('orderlist');
	Route::post('profile/update', 'HomeController@profile_update')->name('profile_update');
	
	Route::any('/taskdecline/{id}', 'HomeController@taskdecline')->name('taskdecline');
	Route::any('/taskaccepted/{id}', 'HomeController@taskaccepted')->name('taskaccepted');
	Route::any('/markascomplete/{id}', 'HomeController@markascomplete')->name('markascomplete');
	Route::any('/addCalendarTime', 'HomeController@addCalendarTime')->name('addCalendarTime');
	Route::any('/cleaner/{id}', 'HomeController@cleanerprofile')->name('cleanerprofile');
	Route::any('/cleaner/calendar/details/{dtd}', 'HomeController@cleanercalendardetails')->name('cleanercalendardetails');	
	
	Route::any('/cleaner/delete/calendar/{id}', 'HomeController@deletecalendar')->name('deletecalendar');	
	Route::post('sendmsg', 'HomeController@sendmsg');	
	Route::any('/msghistory/{id}', 'HomeController@msghistory')->name('msghistory');	
	
	Route::any('/customer/{id}', 'HomeController@customer')->name('customer');	
	Route::any('cleanerCalendarTime', 'HomeController@cleanerCalendarTime');	
	Route::any('cleanerCalendarAvailableTime', 'HomeController@cleanerCalendarAvailableTime');	
	Route::any('getWorkOrder/{id}', 'HomeController@getWorkOrder');	
	Route::any('selectCleanerWorkOrder/{cleanerId}/{workorder_id}', 'HomeController@selectCleanerWorkOrder');	
	


	Route::any('testmail', 'HomeController@testmail');
	
	
});
Route::group(['prefix' => 'admin', 'middleware' => ['web','auth'],'before'=>'auth','after'=>'no-cache'], function (){
	Route::resource('location','LocationController');
	Route::resource('user','UserController');
	Route::get('calendar', 'UserController@calendar')->name('calendar');
	Route::get('calendar-master', 'UserController@calendarmaster')->name('calendarmaster');
	Route::get('cleaner', 'UserController@cleaner')->name('cleaner');
	Route::post('/cleaner/slot/create', 'UserController@slot_create')->name('slot_create');
	Route::post('/cleaner/slot/resize', 'UserController@slot_resize')->name('slot_resize');
	Route::post('/cleaner/slot/move', 'UserController@slot_move')->name('slot_move');
	Route::get('orderlist', 'UserController@orderlist')->name('orderlist');
	Route::get('orderlist/cancel', 'UserController@cancel')->name('orderlist.cancel');
	Route::get('orderlist/payment/{id}', 'UserController@payment')->name('orderlist.payment');
});

Route::get('google', function () {
    return view('googleAuth');
});
    
Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('google-login', 'Auth\LoginController@handleGoogleCallback');

Route::get('auth/google/login', 'Auth\LoginController@redirectToGoogleNew');
Route::get('google_login', 'Auth\LoginController@handleGoogleCallbackNew');


Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');
