<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CleanerScheduleCalendar extends Model
{
	protected $table = 'schedule_cleaner_calendar';
    public $fillable = ['cleaner_id','status','slot_start_time','slot_end_time'];
}