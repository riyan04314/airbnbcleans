<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendgridEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = 'support@69ideas.com';
        $subject = $this->data['subject'];
        $name = $this->data['name'];
        /*
		->cc($address, $name)
		->bcc($address, $name)                    		
		*/
	   
        return $this->view('emails.test')
                    ->from($address, $name)
					->bcc('ritadas20@gmail.com', $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'test_message' => $this->data['message'] ]);
    }
}
