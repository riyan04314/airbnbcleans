<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Mail\SendgridEmail;
use Socialite;
use Auth;
use Exception;
use App\User;
   
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
   
    use AuthenticatesUsers;
   
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {		
        $this->middleware('guest')->except('logout');
    }
   
    public function redirectToGoogle(Request $request)
    {
		$type = $request->type;
		if($type == 'login'){
			session(['socialtype'=>'login']);
		}
		else{
			session(['socialtype'=>'registration']);
		}
		return Socialite::driver('google')->redirect();
    }
    
 
    public function handleGoogleCallback(Request $request)
    {
		$user = Socialite::driver('google')->user();
		$user = $user->user;		
		$type = $request->type;
		//echo session('socialtype');exit;
		//dd($user);
		//$finduser = User::where('google_id', $user['id'])->first();
		$finduser = User::where('email', $user['email'])->first();
		//dd($user,$finduser);
		if($finduser){
			Auth::login($finduser);
			return redirect('/home');
		}
		else{
			if(session('socialtype') == 'login'){
				return redirect('/register');	
			}
			else{
				$newUser = User::create([
					'type' => 3,
					'name' => $user['name'],
					'email' => $user['email'],
					'google_id'=> $user['id'],
					'email_verified_at'=> date('Y-m-d H:i:s')
				]);
				//dd(22,$newUser);
				Auth::login($newUser);
				return redirect('home');
			}			
		}
		
		/*try {
		
        } catch (Exception $e) {
			//dd(99);
            return redirect('auth/google');
        }*/
    }
	
	public function redirectToGoogleNew()
    {
        return Socialite::driver('google')->with(['redirect_uri'=>'https://airbnbcleans.com/google_login','user'])->redirect();
    }
   
    public function handleGoogleCallbackNew()
    {
		
		print_r($_POST);
		$user = Socialite::driver('google')->user();
		dd(6667,$user);
		$user = $user->user;
		//dd($user->user['id']);
		$finduser = User::where('google_id', $user['id'])->first();
		//dd($finduser);
		if($finduser){
			Auth::login($finduser);
			return redirect('/home');
		}
		else{
			return redirect('/register');
		}
		
		/*try {
		
        } catch (Exception $e) {
			//dd(99);
            return redirect('auth/google');
        }*/
    }
	 	
	public function authenticated($request, $user )
    {
		$dtd 		= date('d/m/Y h:i A');
		$emailid 	= $user['email'];
		if(in_array($emailid,array('cleaner0047@gmail.com','cleaner03@gmail.com','cleaner04@gmail.com','cleaner044@gmail.com','cleaner046@gmail.com','customer003@gmail.com','customer01@gmail.com','customer019@gmail.com','customer02@gmail.com','customer03@gmail.com','customer04@gmail.com','customer05@gmail.com','ritadasdff@gmail.com','test@gmail.com','test222222q@gmail.com','testCleaner@gmail.com','tester4357912@gmail.com','testtttgg@gmail.com','ritadasxsf@gmail.com'))){
			$emailid	= 'riyan04314@gmail.com';
		}
		
		
		$data 	= ['name'=>$user['name'],'subject'=>'AirbnbCleans Login','message' => 'You successfully login in AirbnbCleans on '.$dtd.' from IP '.request()->ip()];
		\Mail::to($emailid)->send(new SendgridEmail($data));
        return redirect()->intended($this->redirectPath());
    }	
		
}