<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Address;
use App\Subscription;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
		if(!in_array($data['type'],array(1,3))){
			unset($data['type']);
		}
		return Validator::make($data, [
            'type' => ['required', 'string', 'max:1'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
		
    }

    protected function create(array $data)
    {
		$res = User::create([
            'name' => $data['name'],
            'type' => $data['type'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
			'address' => $data['address'],
			'phone_number' => $data['phone_number']
        ]);
		
		if(isset($data['stripe_emailid']) && $data['type'] == 1){
			Subscription::where('user_id',0)->where('name',$data['stripe_emailid'])->update(array('user_id'=>$res->id));
		}
		//dd($data,$res);
		$inputdata['user_id'] 			= $res->id;
		//$inputdata['location_id'] 		= $data['location_id'];
		$inputdata['address_details'] 	= $data['address'];
		Address::insert($inputdata);		
		
		
		
		if($data['type'] == 3)
		{
			$clr = $res->id;
			$i = 1;
			$y = 2020;
			while($i<15){
				$i++;
				$m = $i;
				if($m>12){
					$m = $m-12;
					$y = 2021;
				}
				
				$j = 0;
				$maxdtd = 31;

				$maxdtd = 29;
				if(in_array($m,array(1,3,5,7,8,10,12))){
					$maxdtd = 31;
				}
				elseif(in_array($m,array(4,6,9,11))){
					$maxdtd = 30;
				}				

				if($m == 2  ){
					$maxdtd = 28;
					if($i < 12){
						$maxdtd = 29;
					}
				}
				
				
				$sql = "INSERT INTO `cleaner_calendar_slot` (`cleaner_id`, `dtd`, `slot`) VALUES ";
				while($j<$maxdtd){
					$j++;
					if(strlen($j) == 1){
						$k = '0'.$j;
					}
					else{
						$k = $j;
					}
					$sql .= "
					(".$clr.",  '".$y."-0".$m."-".$k."', '7.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '8.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '9.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '10.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '11.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '12.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '13.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '14.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '15.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '16.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '17.0'), 
					(".$clr.",  '".$y."-0".$m."-".$k."', '18.0'),
					(".$clr.",  '".$y."-0".$m."-".$k."', '19.0'),";
				}
				$sql = substr($sql,0,-1);
				//mysqli_query($con,$sql);
				DB::insert($sql);
				//echo $sql.'<br><hr><br>';
			}
		}
		//echo $sql;
		//exit;
		
		return $res;
    }	
}
