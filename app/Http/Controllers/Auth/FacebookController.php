<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Socialite;
use Exception;
use Auth;


class FacebookController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook(Request $request)
    {
		$type = $request->type;
		if($type == 'login'){
			session(['socialtype'=>'login']);
		}
		else{
			session(['socialtype'=>'registration']);
		}		
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
			$user = $user->user;
			
			if($user['id'] == "1975710852462828"){
				$user['name'] 	= "Tester 84";
				$user['email']	= "tester4357912@gmail.com";
				$user['id']		= 1975710852462998;
			}
			
			
			
			//$finduser = User::where('facebook_id', $user['id'])->first();
			$finduser = User::where('email', $user['email'])->first();
			//dd($user,$finduser);
			if($finduser){
				Auth::login($finduser);
				return redirect('/home');
			}
			else{
				if(session('socialtype') == 'login'){
					return redirect('/register');	
				}
				else{											
					$create['name'] 				= $user['name'];
					$create['email'] 				= $user['email'];
					$create['facebook_id'] 			= $user['id'];
					$create['type']					= 3;
					$create['email_verified_at']	= date('Y-m-d H:i:s');			
					$userModel 			= new User;
					$createdUser 		= $userModel->addNew($create);
					Auth::loginUsingId($createdUser->id);
					return redirect()->route('home');
				}
			}			
        }
		catch (Exception $e) {
            return redirect('auth/facebook');
        }
    }
}
