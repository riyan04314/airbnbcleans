<?php
// SubscriptionController.php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class SubscriptionController extends Controller
{
public function create(Request $request, \App\Plan $plan)
{
    $plan = \App\Plan::findOrFail($request->get('plan'));
    $user = $request->user();
    $paymentMethod = $request->paymentMethod;

    $user->createOrGetStripeCustomer();
    $user->updateDefaultPaymentMethod($paymentMethod);
    $user
        ->newSubscription('main', $plan->stripe_plan)
        ->trialDays(7)
        ->create($paymentMethod, [
            'email' => $user->email,
        ]);

    return redirect()->route('home')->with('status', 'Your plan subscribed successfully');
}
    
	
	
	public function createold(Request $request, Plan $plan)
    {
		echo '----'.$request->get('plan').'<hr>';
        $plan = Plan::findOrFail($request->get('plan'));
        
		echo '-----'.$plan->stripe_plan.'<hr>';
        $request->user()
            ->newSubscription('main', $plan->stripe_plan)
            ->create($request->stripeToken);
        
        return redirect()->route('home')->with('success', 'Your plan subscribed successfully');
    }
}

