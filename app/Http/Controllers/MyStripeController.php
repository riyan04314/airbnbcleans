<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect,Response;
use Stripe\Stripe;
use App\Subscription;
use Auth;

class MyStripeController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payment');
    }
      
     
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//echo '<pre>';print_r($_REQUEST);exit;
		Stripe::setApiKey(config("data.STRIPE_SECRET"));		
		$customer = \Stripe\Customer::create(array(
				"email" => $request->email,
				"source" => $request->tokenId,
			));
			
			
        $res = \Stripe\Subscription::create(array(
            "customer" => $customer->id,
            "plan" => $request->plan,
        ));
		$res->emailid = $request->email;
		
		$qty = 1;
		if($res->plan->id == config('data.onetimeclean')){
			$qty = 1;
		}
		elseif($res->plan->id == config('data.expressclean')){
			$qty = 5;
		}
		elseif($res->plan->id == config('data.jumboclean')){
			$qty = 10;
		}
		
		$uid = 0;
		if(isset(Auth::user()->id)){
			$uid = Auth::user()->id;
		}
		
		$insdata['user_id']			= $uid;
		$insdata['name']			= $request->email;
		$insdata['stripe_id'] 		= $res->id;
		$insdata['stripe_status']	= 'PaymentDone';
		$insdata['stripe_plan']		= $res->plan->id;
		$insdata['quantity']		= $qty;
		$insdata['trial_ends_at']	= date('Y-m-d',strtotime("+30 days"));
		$insdata['ends_at']			= date('Y-m-d',strtotime("+30 days"));
		$insdata['created_at']		= date('Y-m-d H:i:s');
		$insdata['updated_at']		= date('Y-m-d H:i:s');
		//dd($insdata);
		Subscription::insert($insdata);
		return $res;
    }
}
