<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CleanerCalendar extends Model
{
	protected $table = 'cleaner_calendar';
    public $fillable = ['cleaner_id','dtd','start_time','end_time','status'];
}


