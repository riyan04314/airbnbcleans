<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Address extends Model
{
	protected $table = 'address';
    public $fillable = ['user_id','location_id','address_details'];
}