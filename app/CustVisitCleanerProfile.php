<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CustVisitCleanerProfile extends Model
{
	protected $table = 'cust_visit_cleaner_profile';
    public $fillable = ['cleaner_id','customer_id'];
}


