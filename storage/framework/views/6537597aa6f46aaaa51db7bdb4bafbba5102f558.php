<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no">
<title><?php echo e(config('data.APP_NAME'), false); ?></title>
<link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap-slider.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/slick.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/style.css">
<link rel="stylesheet" type="text/css" href="/frontend/css/custom.css">
</head>

<body class="fullpage">
<div id="form-section" class="container-fluid signin">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo" style="width:62px;height:18px"></div>
        </a>
    </div>
    <div class="row">
        <div class="info-slider-holder">
            <div class="info-holder">
                <h6>A Service you can anytime modify.</h6>
                <div class="bold-title">it’s not that hard to get<br>
    a website <span>anymore.</span></div>
                <div class="mini-testimonials-slider">
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="images/person1.jpg" alt="">
                            <h4>Chris Walker</h4>
                            <h5>CEO & CO-Founder @HelloBrandio</h5>
                            <p>“In hostify we trust. I am with them for over
    7 years now. It always felt like home!
    Loved their customer support”</p>
                        </div>
                    </div>
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="images/person2.jpg" alt="">
                            <h4>Chris Walker</h4>
                            <h5>CEO & CO-Founder @HelloBrandio</h5>
                            <p>“In hostify we trust. I am with them for over
    7 years now. It always felt like home!
    Loved their customer support”</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-holder">
            <div class="menu-holder">
                <ul class="main-links">
                    <li><a class="normal-link" href="/register">Don’t have an account?</a></li>
                    <li><a class="sign-button" href="/register">Sign up</a></li>
                </ul>
            </div>
            <div class="signin-signup-form">
                <div class="form-items">
                    <div class="form-title">Sign in to your account</div>
                    <form id="signinform" method="POST" action="<?php echo e(route('login'), false); ?>">					
                        <?php echo csrf_field(); ?>
						<div class="form-text">
								<input id="email" placeholder="Email Id" type="email" class="<?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" value="<?php echo e(old('email'), false); ?>" required autocomplete="email" autofocus>

                                <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message, false); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>							
                        </div>
                        <div class="form-text">
                           
                                <input id="password" value="12345678" placeholder="Password" type="password" class="<?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required autocomplete="current-password">

                                <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message, false); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>							
                        </div>
                        					
                        <div class="form-button">
                            <button id="submit" type="submit" class="ybtn ybtn-accent-color">Sign in</button>
							<input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : '', false); ?>>
                                    <label class="form-check-label" for="remember">
                                        <?php echo e(__('Remember Me'), false); ?>

                                    </label>
                                <?php if(Route::has('password.request')): ?>
                                    <a class="btn btn-link" href="<?php echo e(route('password.request'), false); ?>">
                                        <?php echo e(__('Forgot Your Password?'), false); ?>

                                    </a>
                                <?php endif; ?>
                        </div>
                    </form>					
					
                </div>
				
         <div class="col-md-12 row-left">
			<div class="col-md-6 float-left" style="padding-left:15px;">
			  <a href="<?php echo e(url('auth/google?type=login'), false); ?>">
					<img src="/google-login-btn.png" width="200" />
			  </a>
			  </div>
			  <div class="col-md-6 float-right">
			  <a href="<?php echo e(url('auth/facebook?type=login'), false); ?>" >
					<img src="/facebook-sign-in-button.png" width="200" />
				</a>     
			 </div>
		 </div>			
            </div>
        </div>
    </div>
</div>
<script src="/frontend/js/jquery.min.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/js/bootstrap-slider.min.js"></script>
<script src="/frontend/js/slick.min.js"></script>
<script src="/frontend/js/main.js"></script>
</body>
</html>
<?php /**PATH D:\server734\htdocs\projects\laravel\airbnb\resources\views/auth/login.blade.php ENDPATH**/ ?>