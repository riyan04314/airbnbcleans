@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Locations</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('location.create') }}"> Create New Location</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>No</th>
            <th>Location Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
    @foreach ($locations as $key => $location)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $location->name }}</td>
        <td>{{ $location->description }}</td>
        <td>
            <a class="btn btn-primary" href="{{ route('location.edit',$location->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['location.destroy', $location->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>


    {!! $locations->render() !!}

		</div>
	</div>
</div>
@endsection