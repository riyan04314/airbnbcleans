<?php //print_r(config('data.onetimeclean'));exit;?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
	<head>
		<!-- Basic Page Needs -->
		<meta charset="UTF-8">
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<title>House Clean</title>

		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="author" content="riyan04314@gmail.com">

		<!-- Mobile Specific Metas -->
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- Boostrap style -->
		<!-- <link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap.min.css"> -->

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/bootstrap4-alpha3.min.css">

		<!-- FONTS-->
		<link rel="stylesheet" href="http://fonts.googleapis.com/icon?family=Material+Icons">

		<!-- Theme style -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/style.css">

		<!-- Calendar -->
		<link href='/userdashboard/stylesheets/fullcalendar.min.css' rel='stylesheet' />
		<link href='/userdashboard/stylesheets/fullcalendar.print.min.css' rel='stylesheet' media='print' />

		<!-- Responsive -->
		<link rel="stylesheet" type="text/css" href="/userdashboard/stylesheets/responsive.css">

		<!-- Favicon -->
	    <link href="/userdashboard/images/icon/favicon.png" rel="shortcut icon">
	</head>
	<body>

		<!-- Loader -->
		<div class="loader">
		  	<div class="inner one"></div>
		  	<div class="inner two"></div>
		  	<div class="inner three"></div>
		</div>

		<header id="header" class="header fixed">
			<div class="navbar-top">
				<div class="curren-menu info-left">
					<div class="logo">
						<a href="/" title="">
							<img src="/userdashboard/img/logo.png" alt="One Admin">
						</a>
					</div><!-- /.logo -->
					<div class="top-button">
						<span></span>
					</div><!-- /.top-button -->					
				</div><!-- /.curren-menu -->
				<ul class="info-right">
					<li class="setting">
						<a href="#" class="waves-effect waves-teal" title="">
							<img src="/userdashboard/img/icon/setting.png" alt="">
						</a>
					</li><!-- /.setting -->
					<li class="notification">
						<a href="#" class="waves-effect waves-teal" title="">
							7
						</a>
					</li><!-- /.notification -->
					<li class="user">
						<div class="avatar">
							<img src="/userdashboard/img/avatar/01.png" alt="">
						</div>
						<div class="info">
							<p class="name">{{ Auth::user()->name }}</p>
							<p class="address">San Fransico, CA</p>
						</div>
						<div class="arrow-down">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
							<i class="fa fa-angle-up" aria-hidden="true"></i>
						</div>
						<div class="dropdown-menu">
							<ul>
								<li>
									<div class="avatar d-none">
										<img src="/userdashboard/img/avatar/01.png" alt="">
									</div>
									<div class="clearfix"></div>
								</li>
								<li>
									<a href="#" class="waves-effect" onclick="javascript:$('.setting').click()" title="">My Account</a>
								</li>
								<li>
									<a class="waves-effect" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>									
								</li>
							</ul>
						</div><!-- /.dropdown-menu -->
						<div class="clearfix"></div>
					</li><!-- /.user -->
					<li class="button-menu-right">
						<img src="/userdashboard/img/icon/menu-right.png" alt="">
					</li><!-- /.button-menu-right -->
				</ul><!-- /.info-right -->
				<div class="clearfix"></div>
			</div>	<!-- /.navbar-top -->
		</header><!-- /header <-->

		<section class="vertical-navigation left">
			<div id="ex1" class="modal">
				<p>Thanks for clicking. That felt good.</p>
				<a href="#" rel="modal:close">Close</a>
			</div>
			<a href="#ex1" id="modalhref" style="display:none;" rel="modal:open">Open Modal</a>


		
			<div class="user-profile">
				<div class="user-img">
					<a href="#" title="">
						<div class="img">
							<img src="/userdashboard/img/avatar/avatar-dashboard.png" alt="">
						</div>
						<div class="status-color blue heartbit style1"></div>
					</a>
				</div>
				<ul class="user-options">
					<li class="name"><a href="#" title="">{{ Auth::user()->name }}</a></li>
					<li class="options">ADMINISTRATOR</li>
				</ul>
			</div>
			<ul class="sidebar-nav">
				<li class="dashboard waves-effect waves-teal active">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/monitor.png" alt="">
					</div>
					<span>DASHBOARD</span>
				</li>
				<li class="pages waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/pages.png" alt="">
					</div>
					<span>Order List</span>
				</li>
				<li class="setting waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/setting-02.png" alt="">
					</div>
					<span>SETTING</span>
				</li>
				
				<li class="message waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/message.png" alt="">
						<span>3</span>
					</div>
					<span>MESSAGE</span>
				</li>
				<li class="calendar waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/calendar.png" alt="">
					</div>
					<span>CALENDAR</span>
				</li>
				<li class="apps waves-effect waves-teal">
					<div class="img-nav">
						<img src="/userdashboard/img/icon/apps.png" alt="">
					</div>
					<span>APPS</span>
				</li>
				
			</ul>
		</section><!-- /.vertical-navigation -->

		<main>
			<section id="dashboard">				
				@if(Auth::user()->type == 1)
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<div class="row stripebtn">
									<div class="col-md-4">
										<button class="btn btn-primary btn-block" onclick="pay(50,'plan_GSQLWvdYPjkcGJ','One Time Cleaning')">One Time Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-success btn-block" onclick="pay(250,'plan_GSQCkEc344j1JU','Express Cleaning')">Express Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-info btn-block" onclick="pay(400,'plan_GSQB4nqOJPNamT','Express Jumbo')">Express Jumbo</button>
									</div>
								</div>	
							</div>
						</div>
					</div>				
				</div>
				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault">
			              	<div class="box-title col-md-12" style="display:none;">
							Available Express Token  is 10, valid from 10-12-2019 to 09-12-2019<hr>
							</div>
							<div class="box-title col-md-6">
								Address &nbsp;
								<select id="selectedLocation" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" onchange="searchcleaner()" >
									@foreach($userAddress as $val)
										<option value="{{$val->id.'_'.$val->location_id}}">{{ $val->address_details }}</option>
									@endforeach
									<?php /*@foreach($locationlist as $val)
										<option value="{{ $val->id}}">{{$val->name}}</option>
									@endforeach*/ ?>
								</select>
							</div>
							
							<div class="box-title col-md-3">
								Dates &nbsp;
								<input id="selectedDate" value="<?php echo date('d/m/Y',strtotime('+3 days'));?>" type="text" class="datepicker" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:100px;;" onchange="searchcleaner()" />
							</div>
							
			              	<div class="box-title col-md-3">
								Time &nbsp;
								<select id="selectedTime" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" onchange="searchcleaner()" >
									<option value="09:00:00-10:30:00">9AM-10.30AM</option>
									<option value="11:00:00-12:30:00">11AM-12.30PM</option>
									<option value="13:00:00-14:30:00">1PM-2.30PM</option>
									<option value="15:00:00-16:30:00">3PM-4.30PM</option>
								</select>
							</div>
							<div class="clearfix"></div>
							
							<div class="clearfix"></div>							
							<div class="box-inbox right" style="height:auto;">
								<h3 class="loading" style="display:none;">Loading....</h3>
								<h3 class="cleanerdiv2" style="display:none;">Available Cleaner List On Selected Date-Time</h3>								
								<div class="box-content cleanerdiv">
									<ul class="inbox-list cleanerlist">
									</ul>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<div class="box-content">
									<ul class="inbox-list">
										<li class="waves-effect">
											<a href="#" title="">
												<div class="left">Schedule Clean Booking List</div>
											</a>
										</li>
										@foreach($bookinglist as $val)
											<li class="waves-effect" id="schedule_{{$val->id}}">
												<a title="">
													<div class="left">
														<img src="/userdashboard/img/avatar/inbox-01.png" alt="">
														<div class="info">
															<p class="name">Cleaner Name : @if(isset($userlist[$val->cleaner_id]->name)){{ $userlist[$val->cleaner_id]->name}}@endif</p>
															<p>Schedule Date : <?php echo date('d-m-Y',strtotime($val->dtd)).', '.substr($val->from_time,0,5).' - '.substr($val->to_time,0,5);?></p>
															<p>Address : @if(isset($addresslist[$val->address_id]->address_details)){{ $addresslist[$val->address_id]->address_details }}@endif</p>
														</div>
													</div>
													<div class="right">
														<input type="button" value="Pending" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="bookcleaner({{ $val->id}})" />
														<input type="button" value="Reschedule" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="bookcleaner({{ $val->id}})" />
														<input type="button" value="Cancel" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="cancelschedule({{ $val->id}})" />
													</div>
													<div class="clearfix"></div>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				@else
				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<h3 class="loading" style="display:none;">Loading....</h3>
								<h3 class="cleanerlist">New Booking Order For Cleaning</h3>								
								<div class="box-content">
									<ul class="inbox-list cleanerlist">
										@foreach($bookinglist as $val)
											<li class="waves-effect">
												<a href="#" title="">
													<div class="left">
														<img src="/userdashboard/img/avatar/inbox-01.png" alt="">
														<div class="info">
															<p class="name">Customer Name : @if(isset($userlist[$val->customer_id]->name)){{ $userlist[$val->customer_id]->name}}@endif</p>
															<p>Schedule Date : <?php echo date('d-m-Y',strtotime($val->dtd)).', '.substr($val->from_time,0,5).' - '.substr($val->to_time,0,5);?></p>
															<p>Address : @if(isset($addresslist[$val->address_id]->address_details)){{ $addresslist[$val->address_id]->address_details }}@endif</p>
														</div>
													</div>
													<div class="right">
														<input type="button" value="Accepted" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="bookcleaner({{ $val->id}})" />
													</div>
													<div class="clearfix"></div>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="rows">
		        	<div class="box box-danger left" style="height:auto;">						
						<div class="box-inbox right" style="height:auto;">
							<h3>My Available Date For Cleaning</h3>								
							<div class="box-content">
								<ul class="inbox-list">
									@php($i=0)
									@while($i<30)
										@php($i++)
									<li class="waves-effect col-md-4">
										<a href="#" title="">
											<div class="left">
												<div class="info">
													<p class="name">{{ date('d/m/Y',strtotime('+'.$i.' days'))}}</p>
												</div>
											</div>
											<div class="left" style="margin-left:25px;">
												<div class="info">
													<p class="name">9AM - 6.30PM</p>
												</div>
											</div>
											<div class="clearfix"></div>
										</a>
									</li>
									@endwhile
								</ul>
							</div>
						</div>
					</div>
				</div>
				@endif
				
			</section>

			<section id="message">
				   	<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<div class="row stripebtn">
									<div class="col-md-4">
										<button class="btn btn-primary btn-block" onclick="pay(50,'plan_GSQLWvdYPjkcGJ','One Time Cleaning')">One Time Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-success btn-block" onclick="pay(250,'plan_GSQCkEc344j1JU','Express Cleaning')">Express Cleaning</button>
									</div>
									<div class="col-md-4">
										<button class="btn btn-info btn-block" onclick="pay(400,'plan_GSQB4nqOJPNamT','Express Jumbo')">Express Jumbo</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
											
		        	<div class="box box-danger left" style="height:auto;">
			            <div class="box-header with-border">
							<div class="box-inbox right" style="height:auto;">
								<div class="box-content">
									<h3>History Order List</h3>
									<ul class="inbox-list">
										@foreach($orderlist as $val)
											<li class="waves-effect">
												<a href="#" title="">
													<div class="left">
														<img src="/userdashboard/img/avatar/inbox-01.png" alt="" style="display:none;">
														<div class="info">
															<p class="name">
																Order Plan : 
																@if(config('data.onetimeclean') == $val->stripe_plan)
																	{{ 'One Time Clean' }}
																@elseif(config('data.expressclean') == $val->stripe_plan)
																	{{ 'Express Clean' }}
																@elseif(config('data.jumboclean') == $val->stripe_plan)
																	{{ 'Jumbo Express' }}
																@endif
															</p>
															<p>Order ID :  {{ $val->stripe_id }}</p>
														</div>
													</div>
													<div class="right">
														<input type="button" value="{{ 'Valid Till Date : '.date('d/m/Y',strtotime($val->ends_at)) }}" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;" />
													</div>
													<div class="clearfix"></div>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
						
			
			
				<div class="box box-message" style="display:none;">
					<div class="box-header">
						<div class="header-title">
							<img src="/userdashboard/img/icon/download.png" alt="">
							<span>INBOX</span>
						</div>
					</div><!-- /.box-header -->
					<div class="box-content">
						<ul class="message-list scroll">
							<?php $i = 0;while($i<10){$i++;?>
							<li class="waves-effect waves-teal">
								<div class="left">
									<div class="avatar">
										<img src="/userdashboard/img/avatar/message-01.png" alt="">
										<div class="status-color blue style2 heartbit"></div>
									</div>
									<div class="content">
										<div class="username">
											<div class="name">
												Jonathan Alex
											</div>
										</div>
										<div class="text">
											<p>Hi, please loock my last design</p>
											<p>I hope you like it.</p>
										</div>
									</div>
								</div><!-- /.left -->
								<div class="right">
									<div class="date">
										Today, 10:15 PM
									</div>
								</div><!-- /.right -->
								<div class="clearfix"></div>
							</li><!-- /li.waves-effect -->
							<?php } ?>
						</ul><!-- /.message-list scroll -->
						<div class="new-message">
							<a href="#" class="waves-effect" title="">Compose New</a>
						</div><!-- /.new-message -->
					</div><!-- /.box-content -->
				</div><!-- /.box box-message -->
				<div class="message-info right"  style="display:none;">
					<div class="message-header">
						<div class="move-message">
							<a href="#" title="">
								<span><img src="/userdashboard/img/icon/bin.png" alt=""></span>
								MOVE TO TRASH
							</a>
						</div><!-- /.move-message -->
						<div class="box-info-messager">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="content">
								<div class="username">
									Ricky Martin
								</div>
								<div class="text">
									<p>Hi, please loock my last design</p>
									<p>I hope you like it.</p>
								</div>
							</div>
						</div><!-- /.box-info-messager -->
					</div><!-- /.message-header -->
					<div class="message-box scroll">
						
						<?php $i = 0;while($i<10){$i++;?>
						<div class="message-in">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-06.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, John</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design.</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-in -->
						<div class="clearfix"></div>
						<div class="message-out">
							<div class="message-pic">
								<img src="/userdashboard/img/avatar/message-07.png" alt="">
								<div class="status-color purple"></div>
							</div>
							<div class="message-body">
								<div class="message-text">
									<p>Hi, Martin</p>
									<p>You have excellent dashboard design, I wanted to offer to cooprate. I can promote your design. to offer to cooprate</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div><!-- /.message-out -->
						<div class="clearfix"></div>
						<?php } ?>
					</div>
					<div class="form-chat">
						<form action="#" method="get" accept-charset="utf-8">
							<div class="message-form-chat">
								<span class="pin">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/pin.png" alt="">
									</a>
								</span><!-- /.pin -->
								<span class="message-text">
									<textarea name="message" placeholder="Type your message..." required="required"></textarea>
								</span><!-- /.message-text -->
								<span class="camera">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/camera.png" alt="">
									</a>
								</span><!-- /.camera -->
								<span class="icon-message">
									<a href="#" title="">
										<img src="/userdashboard/img/icon/icon-message.png" alt="">
									</a>
								</span><!-- /.icon-right -->
								<span class="btn-send">
									<button class="waves-effect" type="submit">Send</button>
								</span><!-- /.btn-send -->
								<div class="icon-mobile">
									<ul>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/pin.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/camera.png" alt=""></a>
										</li>
										<li>
											<a href="#" title=""><img src="/userdashboard/img/icon/icon-message.png" alt=""></a>
										</li>
									</ul>
								</div><!-- /.icon-right -->
							</div><!-- /.message-form-chat -->
							<div class="clearfix"></div>
						</form><!-- /form -->
					</div>
				</div><!-- /.message-info -->
				<div class="clearfix"></div>
			</section><!-- /#message -->

			
			<section id="setting" style="display:none;">
				<div class="rows">
					<div class="box box-danger left" style="height:auto;">
						<div class="box-header with-border customerdefault2">
			              	<div class="box-title col-md-12">
								<h2>Profile Setting</h2><br>
							</div>
					
							
							<select id="selectedLocation" style="display:none; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;" onchange="searchcleaner()" >
								@foreach($userAddress as $val)
									<option value="{{ $val->id.'_'.$val->location_id}}">{{ $val->address_details }}</option>
								@endforeach
							</select>					
							
							<div class="col-md-8 float-left">
							<div class="box-title col-md-12">
								<label class="col-md-4">Name</label>
								<input type="text" value="{{ Auth::user()->name}}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
							</div>
							<div class="box-title col-md-12">
								<label class="col-md-4">Email</label>
								<input type="text" value="{{ Auth::user()->email }}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
							</div>
							<div class="box-title col-md-12">
								<label class="col-md-4">Phone Number</label>
								<input type="text" value="{{ Auth::user()->phone_number }}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
							</div>
							<div class="box-title col-md-12">
								<label class="col-md-4">Profile Image</label>
								<input type="file" value="{{ Auth::user()->phone_number }}" style="margin-bottom:10px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
							</div>
							</div>
							
							
							
							
							<div class="col-md-8 float-left">
								<div class="box-title col-md-12">
									<label class="col-md-4">Address</label>
									<input type="text" value="{{ Auth::user()->phone_number }}" style="margin-bottom:5px; height:30px; padding:7px 12px; color:#000; background-color:#898989; width:60%;"  /><br>								
									<hr>
									<label class="col-md-4">&nbsp;</label><div style="color:#FFF;">Add New Address</div><hr>
								</div>
							</div>
							
							
							
							
						</div>	
					</div>
				</div>
			</section>
			
			
			
			<section id="calendar">
				<div class="breadcrumbs">
					<ul>
						<li><a href="index.html" title="">Home</a></li>
						<li>- Calendar</li>
					</ul>
				</div>
				<div class="box calendar">
					<div class="box-header">
						<h2 class="title float-left">November <span>2018</span></h2>
						<ul class="date float-right">
							<li class="active">MONTH</li>
							<li>YEAR</li>
						</ul>
					</div>
					<div id="box-calendar"></div>
				</div>
				<div class="box" id="external-events">
	                <div class="external-event bg-purple">
	                	<div class="event-title">
	                		<span>9</span> EVENT ONE
	                	</div>
	                	<div class="event-content">
	                		<div class="date">
	                			Tuesday, 9 November, 2018
	                		</div>
	                		<div class="time">
	                			9:00 AM  :  Corporate Meeting
	                		</div>
	                	</div>
	                </div>
	                <div class="external-event bg-aqua">
	                	<div class="event-title">
	                		<span>11</span> EVENT TWO
	                	</div>
	                	<div class="event-content">
	                		<div class="date">
	                			Saturday, 11 November, 2018
	                		</div>
	                		<div class="time">
	                			10:00 AM  :  Business Meeting
	                		</div>
	                	</div>
	                </div>
	                <div class="external-event bg-blue">
	                	<div class="event-title">
	                		<span>11</span> EVENT THREE
	                	</div>
	                	<div class="event-content">
	                		<div class="date">
	                			Wednesday, 15 November, 2018
	                		</div>
	                		<div class="time">
	                			9:00 AM  :  Corporate Meeting
	                		</div>
	                	</div>
	                </div>
	                <div class="external-event bg-pink">
	                	<div class="event-title">
	                		<span>17</span> EVENT FOUR
	                	</div>
	                	<div class="event-content">
	                		<div class="date">
	                			Friday, 17 November, 2018
	                		</div>
	                		<div class="time">
	                			11:00 AM  :  Project Meeting
	                		</div>
	                	</div>
	                </div>
	                <div class="external-event bg-orange">
	                	<div class="event-title">
	                		<span>25</span> EVENT FIVE
	                	</div>
	                	<div class="event-content">
	                		<div class="date">
	                			Saturday, 25 November, 2018
	                		</div>
	                		<div class="time">
	                			10:00 AM  :  Corporate Meeting
	                		</div>
	                	</div>
	                </div>
	            </div>
	            <div class="clearfix"></div>
	            <div class="footer">
            		<div class="copyright">
            			<p>2018 © All Right Researved by Imran Hossain</p>
            		</div>
            		<ul class="menu-ft">
            			<li>
            				<a href="#" title="">About</a>
            			</li>
            			<li>
            				<a href="#" title="">Privacy</a>
            			</li>
            			<li>
            				<a href="#" title="">T&C</a>
            			</li>
            			<li>
            				<a href="#" title="">Purchase</a>
            			</li>
            		</ul>
	            	<div class="clearfix"></div>
	            </div>
			</section><!-- /#calendar -->
			
		</main><!-- /main -->

		<section class="member-status right">
			<div class="sidebar-member">
				<ul class="member-tab">
					<li>
						<i class="fa fa-users" aria-hidden="true"></i>
					</li>
					<li>
						<i class="fa fa-bell" aria-hidden="true"></i>
					</li>
				</ul><!-- /.member-tab -->
				<div class="content-tab">
					<div class="scroll content">
						<ul class="member-list online">
							<li class="member-header">ONLINE</li>
							@foreach($userlist as $val)	
								@if($val->type == 3)
								<li>
									<a href="#" title="">
										<div class="avatar">
											<img src="/userdashboard/img/avatar/02.png" alt="">
											<div class="status-color green heartbit"></div>
										</div>
										<div class="info-user">
											<p class="name">{{$val->name}}</p>
											<p class="options">Rating : 4.8/5, Task Complete : 785</p>
										</div>
										<div class="clearfix"></div>
									</a>		
								</li>
								@endif
							@endforeach
							
						</ul><!-- /.member-list online -->
						
						
					</div><!-- /.content scroll -->
					<div class="content scroll">
						<ul class="notify">
							<li>
								<div class="avatar">
									<img src="/userdashboard/img/avatar/02.png" alt="">
								</div>
								<div class="notify-content">
									Robart Alex has a news post.
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="avatar">
									<img src="/userdashboard/img/avatar/03.png" alt="">
								</div>
								<div class="notify-content">
									Anthony Gomes has a news post.
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="avatar">
									<img src="/userdashboard/img/avatar/04.png" alt="">
								</div>
								<div class="notify-content">
									Robarto Thuan has comment post <a href="#" title="">pages</a>.
								</div>
								<div class="clearfix"></div>
							</li>
							<li>
								<div class="avatar">
									<img src="/userdashboard/img/avatar/09.png" alt="">
								</div>
								<div class="notify-content">
									Alex Morgan liked your new image.
								</div>
								<div class="clearfix"></div>
							</li>
						</ul>
					</div><!-- /.content scroll -->
				</div><!-- /.cotnent-tab -->
			</div><!-- /.sidebar-member -->
		</section><!-- /.member-status -->

		<!-- jQuery 3 -->
		<script src="/userdashboard/javascript/jquery.min.js"></script>

		<!-- Bootstrap 4 -->
		<script src="/userdashboard/javascript/tether.min.js"></script>
		<script src="/userdashboard/javascript/bootstrap4-alpha3.min.js"></script>

		<!-- Map chart  -->
		<script src="/userdashboard/javascript/ammap.js"></script>
		<script src="/userdashboard/javascript/worldLow.js"></script>

		<!-- Morris.js charts -->
		<script src="/userdashboard/javascript/raphael.min.js"></script>
		<script src="/userdashboard/javascript/morris.min.js"></script>

		<!-- Chart -->
		<script src="/userdashboard/javascript/Chart.min.js"></script>

		<!-- Calendar -->
		<script src='/userdashboard/javascript/moment.min.js'></script>
		<script src='/userdashboard/javascript/jquery-ui.js'></script>
		<script src='/userdashboard/javascript/fullcalendar.min.js'></script>

		<script type="text/javascript" src="/userdashboard/javascript/jquery.mCustomScrollbar.js"></script>
		<script src="/userdashboard/javascript/smoothscroll.js"></script>
		<script src="/userdashboard/javascript/waypoints.min.js"></script>
		<script src="/userdashboard/javascript/jquery-countTo.js"></script>
		<script src="/userdashboard/javascript/waves.min.js"></script>
		<script src="/userdashboard/javascript/canvasjs.min.js"></script>

		<script src="/userdashboard/javascript/main.js"></script>	
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		
		<!-- jQuery Modal -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
		
		
		<script>
		$( function() {
			$( ".datepicker" ).datepicker({
				dateFormat: 'dd/mm/yy',
				minDate: new Date(),
				maxDate: '+1M'
			});
			setTimeout(function() { searchcleaner(); }, 2000);			
		} );
		hidecleanerlist = function(){
			$('.cleanerdiv').hide();
			$('.loading').show();
			setTimeout(function() { showcleanerlist(); }, 4000);
		}
		showcleanerlist = function(){
			$('.loading').hide();
			$('.cleanerdiv').show();
		}


		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		searchcleaner = function(){
			var selectedTime		= $('#selectedTime').val();
			var selectedDate		= $('#selectedDate').val();
			var selectedLocation	= $('#selectedLocation').val();
			
			
			//alert(888);
			$('.cleanerdiv').hide();
			$('.loading').show();			
			$.ajax({
				data: {
					"_token": "{{ csrf_token() }}",
					"selectedTime":selectedTime,
					"selectedDate":selectedDate,
					"selectedLocation":selectedLocation
				},
				url:'/searchcleaner',
				dataType: 'json',
				type: 'POST',
				success:function(res){
					var str = '<li class="waves-effect"><a href="#" title=""><div class="left">Available Cleaner List On Selected Date-Time</div></a></li>';
					$.each(res, function( index, value ) {
						str += '<li class="waves-effect"><a href="#" title=""><div class="left"><img src="/userdashboard/img/avatar/inbox-01.png" alt=""><div class="info"><p class="name">'+value.name+'</p><p>Rating : 4.8/5, Task Complete : 785</p></div></div><div class="right"><input type="button" value="Select Cleaner" style="height:30px; padding:7px 12px; color:#000; background-color:#898989; width:auto;"  onclick="bookcleaner('+value.id+')" /></div><div class="clearfix"></div></a></li>'																				
					});
					$('.cleanerlist').html(str);					
					$('.loading').hide();		
					$('.cleanerdiv').show();					
				}
			});
			
		}
		bookcleaner = function(){
			var cleanerId 			= arguments[0];
			var selectedTime		= $('#selectedTime').val();
			var selectedDate		= $('#selectedDate').val();
			var selectedLocation	= $('#selectedLocation').val();


			$('.customerdefault').html('Please wait your request in processing in our system.');					
			
			$.ajax({
				type: 'POST',
				data: {
					"_token": "{{ csrf_token() }}",
					"cleanerId":cleanerId,
					"selectedTime":selectedTime,
					"selectedDate":selectedDate,
					"selectedLocation":selectedLocation
				},
				url:'/bookcleaner/'+arguments[0],
				dataType: 'json',
				success:function(res){
					if(res.status == 1){
						$('.customerdefault').html(res.msg);
			            location.reload(true);
					}
					else{
						alert('Selected cleaner not avail at selected date and time, Please select other cleaner!');
					}
				}
			});			
		}
		</script>


<script src="https://checkout.stripe.com/checkout.js"></script>
<script type="text/javascript">
$(document).ready(function () {  
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
});

cancelschedule = function(){
	if(confirm('Are you sure to cancel this cleaning schedule ?')){	
		$('#schedule_'+arguments[0]).hide();
		$.ajax({
			type: 'POST',
			data: {
				/*"_token": "{{ csrf_token() }}",*/
			},
			url:'/cancelschedule/'+arguments[0],
			dataType: 'json',
			success:function(res){
				alert(res.msg);
			}
		});
	}
	
}




  function pay(amount,plan,description) {
    var handler = StripeCheckout.configure({
      key: 'pk_test_4V5q3TkFoDgi2nloUPnjdLPL004PeEOnwo', // your publisher key id
      locale: 'auto',
      token: function (token) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        console.log('Token Created!!');
        console.log(token)
        $('#token_response').html(JSON.stringify(token));

        $.ajax({
          url: '{{ route('stripe.store') }}',
          method: 'post',
		  dataType: 'JSON',
          data: { email:token.email, tokenId: token.id, amount: amount, plan:plan, description:description },
          success: (response) => {
			  
			$('.stripebtn').html('<div class="col-md-12"><div class="btn btn-success btn-block" >Payment complete successfully!<br>Order ID : '+response.id+'</div></div>');
		  },
          error: (error) => {
            console.log(error);
            alert('There is an error in processing.')
          }
        })
      }
    });
   
    handler.open({
      name: 'House Clean',
      description: description,
      amount: amount * 100
    });
  }
</script>		
		
	</body>
</html>