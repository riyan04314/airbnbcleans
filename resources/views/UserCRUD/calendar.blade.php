@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Calendar Master11</h2>
            </div>
            <div class="float-right">
				<select id="location_id" name="location_id" onchange="updatelocation()" class="form-control">
					@foreach(locationlist() as $val)
						<option value="{{ $val['id'] }}"  @if($location_id == $val['id']){{ 'selected' }}@endif>
							{{ $val['name'] }}
						</option>
					@endforeach				
				</select>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

	<div class="row border border-primary bg-white p-2">	
    	<link type="text/css" rel="stylesheet" href="/calendar/media/layout.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_g.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_green.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_traditional.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_transparent.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_white.css" />

		<!-- helper libraries -->
		<script src="/calendar/js/jquery-1.9.1.min.js" type="text/javascript"></script>	
		<!-- daypilot libraries -->
        <script src="/calendar/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
        <div class="main">

            <div style="float:left; width: 160px;">
                <div id="nav"></div>
            </div>
            <div style="margin-left: 160px;">

                <div class="space d-none">
                    Theme: <select id="theme">
                        <option value="calendar_default">Default</option>
                        <option value="calendar_white">White</option>
                        <option value="calendar_g">Google-Like</option>
                        <option value="calendar_green">Green</option>
                        <option value="calendar_traditional">Traditional</option>
                        <option value="calendar_transparent">Transparent</option>
                    </select>
                </div>

                <div id="dp"></div>
            </div>

            <script type="text/javascript">
				updatelocation = function(){
					var location_id = $('#location_id').val();
					window.location.href = '/admin/calendar?location_id='+location_id;
				}
			
                var nav = new DayPilot.Navigator("nav");
                nav.showMonths = 1;
                nav.skipMonths = 1;
                nav.selectMode = "week";
                nav.onTimeRangeSelected = function(args) {
                    dp.startDate = args.day;
                    dp.update();
                    loadEvents();
                };
                nav.init();

                var dp = new DayPilot.Calendar("dp");
                dp.viewType = "Week";
				dp.theme = "calendar_green";
				dp.businessBeginsHour = 10;
				dp.cellWidth = 60;				
                dp.eventDeleteHandling = "Update";
				dp.startDate = "<?php echo date('Y-m-d');?>";
                dp.onEventDeleted = function(args) {
                    $.post("backend_delete.php",
                        {
                            pid: args.e.id(),
							start: args.start
                        },
                        function() {
                            console.log("Deleted.");
                        });
                };

                dp.onEventMoved = function(args) {
					
                };

                dp.onEventResized = function(args) {
					
                };

                // event creating
                dp.onTimeRangeSelected = function(args) {
					
                };

                dp.onEventClick = function(args) {
                    //alert("clicked: " + args.e.id());
                };

                dp.init();

                loadEvents();

                function loadEvents() {
                    dp.events.load("backend_events.php");
                }
				
				
				@foreach($calendar as $val)
					var e = new DayPilot.Event({
						start: new DayPilot.Date("{{ $val['slot_start_time'] }}"),
						end: new DayPilot.Date("{{ $val['slot_end_time'] }}"),
						id: DayPilot.guid(),
						text: "Available"
					});
					dp.events.add(e);
				@endforeach
            </script>

            <script type="text/javascript">
            $(document).ready(function() {
                $("#theme").change(function(e) {
                    dp.theme = this.value;
                    dp.update();
                });
            });
            </script>

        </div>
        <div class="clear">
        </div>


	
	
	
	</div>
	
		</div>
	</div>
</div>

@endsection