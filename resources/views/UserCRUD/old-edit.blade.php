@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Cleaner Details</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('cleaner') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row border border-primary bg-white p-2">
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Name : {{ $user['name'] }}</strong>                
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Email Id : {{ $user['email'] }}</strong>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Phone Number : {{ $user['phone_number'] }}</strong>
            </div>
        </div>
		<div class="col-xs-4 col-sm-4 col-md-4">
			<div class="form-group">
				<strong>Location : 
				{{ $address[0]['location_id'] }}
					@foreach(locationlist() as $val)
						@if(!empty($address[0]['location_id']) && $val['id'] == $address[0]['location_id'])
							{{ $val['name'] }}
						@endif
					@endforeach
				</strong>
			</div>			
		</div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>
					Address : 
					@if(!empty($address[0]['address_details']))
						{{ $address[0]['address_details'] }}
					@endif
				</strong>
            </div>
        </div>
    </div>

	
	<div class="row border border-primary bg-white p-2">
    	<link type="text/css" rel="stylesheet" href="/calendar/media/layout.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_g.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_green.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_traditional.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_transparent.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_white.css" />
		<!-- helper libraries -->
		<script src="/calendar/js/jquery-1.9.1.min.js" type="text/javascript"></script>	
		<!-- daypilot libraries -->
        <script src="/calendar/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
        <div class="main">
            <div style="float:left; width: 160px;">
                <div id="nav"></div>
            </div>
            <div style="margin-left: 160px;">

                <div class="space d-none">
                    Theme: <select id="theme">
                        <option value="calendar_default">Default</option>
                        <option value="calendar_white">White</option>
                        <option value="calendar_g">Google-Like</option>
                        <option value="calendar_green">Green</option>
                        <option value="calendar_traditional">Traditional</option>
                        <option value="calendar_transparent">Transparent</option>
                    </select>
                </div>
                <div id="dp"></div>
            </div>
            <script type="text/javascript">
                var nav = new DayPilot.Navigator("nav");
                nav.showMonths = 1;
                nav.skipMonths = 1;
                nav.selectMode = "week";
                nav.onTimeRangeSelected = function(args) {
                    dp.startDate = args.day;
                    dp.update();
                    loadEvents();
                };
                nav.init();

                var dp = new DayPilot.Calendar("dp");
                dp.viewType = "Week";
				dp.theme = "calendar_green";
				dp.businessBeginsHour = 10;
				dp.cellWidth = 60;					
                dp.eventDeleteHandling = "Update";
				dp.startDate = "<?php echo date('Y-m-d');?>";
                dp.onEventDeleted = function(args) {
                    $.post("backend_delete.php",
                        {
                            pid: args.e.id(),
							start: args.start
                        },
                        function() {
                            console.log("Deleted.");
                        });
                };

                dp.onEventMoved = function(args) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});	                   

				   $.post("/admin/cleaner/slot/move",
                            {
                                id: args.e.id(),
								user_id: {{ $user['id'] }},
                                newStart: args.newStart.toString(),
                                newEnd: args.newEnd.toString()
                            },
                            function() {
                                console.log("Moved.");
                            });
                };

                dp.onEventResized = function(args) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});					
                    $.post("/admin/cleaner/slot/resize",
                            {
                                id: args.e.id(),
                                user_id: {{ $user['id'] }},
								newStart: args.newStart.toString(),
                                newEnd: args.newEnd.toString()
                            },
                            function() {
                                console.log("Resized.");
                            });
                };

                // event creating
                dp.onTimeRangeSelected = function(args) {
					//alert(args.start);
                    //alert(args.start);
                    //var name = prompt("New event name:", "Available");
                    var name = "Available";
                    dp.clearSelection();
                    if (!name) return;
                    var e = new DayPilot.Event({
                        start: args.start,
                        end: args.end,
                        id: DayPilot.guid(),
                        resource: args.resource,
                        text: name
                    });
                    dp.events.add(e);
					
					
					
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
                    $.post("/admin/cleaner/slot/create",
                            {
                                id: e.id,
								start: args.start.toString(),
                                end: args.end.toString(),
                                name: name,
								user_id: {{ $user['id'] }},
								location_id:{{ $address[0]['location_id'] }}
                            },
                            function() {
                                console.log("Created.");
                            });

                };

                dp.onEventClick = function(args) {
                    //alert("clicked: " + args.e.id());
                };

                dp.init();

                loadEvents();

                function loadEvents() {
                    dp.events.load("backend_events.php");
                }
				
				
				@foreach($calendar as $val)
					var e = new DayPilot.Event({
						start: new DayPilot.Date("{{ $val['slot_start_time'] }}"),
						end: new DayPilot.Date("{{ $val['slot_end_time'] }}"),
						id: DayPilot.guid(),
						text: "Available"
					});
					dp.events.add(e);
				@endforeach
            </script>

            <script type="text/javascript">
            $(document).ready(function() {
                $("#theme").change(function(e) {
                    dp.theme = this.value;
                    dp.update();
                });
            });
            </script>

        </div>
        <div class="clear">
        </div>
	</div>
	
		</div>
	</div>
</div>

@endsection