@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Cleaning Order List</h2>
            </div>
            <div class="float-right d-none">
                <a class="btn btn-success" href="{{ route('user.create') }}"> Create New User</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

	
    <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>No</th>
            <th>Customer Details</th>
            <th>Cleaner Details</th>
            <th>Order Details</th>
            <?php /*<th>Ordered Date</th>
			<th>Paid Amt.</th>*/ ?>			
			<th>Scheduled Date</th>
			<th>Status</th>
			<th>Action</th>
        </tr>
		@php($i = 0)
		
		@foreach ($bookinglist as $key => $val)
			@if($val->cleaner_id == 0)
				@php($val->cleaner_id = 2)
			@endif
		<tr>
			<td>{{ ++$i }}</td>
			<td>
				{{ $userlist[$val->customer_id]->name }}<br>
				{{ $userlist[$val->customer_id]->email }}				
			</td>
			<td>
				<a href="{{ asset('/admin/user/'.$val->customer_id.'/edit')}}">{{ $userlist[$val->cleaner_id]->name }}</a><br>
				{{ $userlist[$val->cleaner_id]->email }}
			</td>
			<td>
				Job ID : {{$val->id}}<br>
				City : @if(isset($locationlist[$val->location_id])){{ $locationlist[$val->location_id]->name }}@endif<br>
				Address : <?php /*{{ $addresslist[$val->address_id]->address_details }}*/?>
			</td>
			<td>
				{{ date('d/m/Y',strtotime($val->dtd)) }}<br>
				{{ substr($val->from_time,0,5) }} - {{ substr($val->to_time,0,5) }}
			</td>
			<td>
				@if($val->status == 1)
					Pending
				@elseif($val->status == 2)
					Completed
				@elseif($val->status == 3)
					Cancel by User
				@elseif($val->status == 4)
					Accepted by Cleaner
				@elseif($val->status == 5)
					Canceled by Cleaner
				@elseif($val->status == 6)
					Cancel by Admin
				@elseif($val->status == 7)
					Reschedule by User	
				@elseif($val->status == 12)
					Paid ( {{date('d/m/Y h:i A',strtotime($val->updated_at))}} )
					Paid Amount : ${{$val->cleaner_amt}}
				@endif
				
				@if($val->rating)
					<br>Review : {{$val->rating}}
					@php($i = 0)
					@while($val->rating > $i)
						@php($i++)
						<?php /*<i class="fa fa-star" style="font-size: large;" ></i>
						<i class="icon-ln icon-ln-star"></i>*/ ?>
					@endwhile
				@endif
			</td>
			<td>
				@if($val->status == 1)
					<a class="btn btn-sm btn-primary" href="{{ route('orderlist.cancel',$val->id) }}">Cancel</a>
				@elseif($val->status == 2)
					<a class="btn btn-sm btn-primary" href="{{ route('orderlist.payment',$val->id)}}">Mark as Paid of ${{$val->cleaner_amt}}</a>
				@endif
			</td>
		</tr>
		@endforeach
    </table>
    <?php /*{!! $user->render() !!}*/?>
		</div>
	</div>
</div>
@endsection