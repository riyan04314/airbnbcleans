@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Cleaner List</h2>
            </div>
            <div class="float-right d-none">
                <a class="btn btn-success" href="{{ route('user.create') }}"> Create New User</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Contact Number</th>
            <th>Address</th>
            <th width="280px">Action</th>
        </tr>
		@foreach ($users as $key => $user)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $user->name }}</td>
			<td>
				{{ $user->phone_number }}<br>
				<a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
			</td>
			<td>
				@if(isset($address[$user->id]))
				@foreach($address[$user->id] as $val)
					City : {{ $location[$val['location_id']]}}<br>
					Address : {{ $val['address_details']}}
				@endforeach
				@endif
			</td>
			<td>
				<a class="btn btn-sm btn-primary" href="{{ route('user.edit',$user->id) }}">Edit</a><?php /**/ ?>
				{!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->id],'style'=>'display:inline']) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
				{!! Form::close() !!}
			</td>
		</tr>
		@endforeach
    </table>


    <?php /*{!! $user->render() !!}*/?>

		</div>
	</div>
</div>
@endsection