@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Calendar Master</h2>
            </div>
            <div class="float-right d-none">
                <a class="btn btn-success" href="{{ route('user.create') }}"> Create New User</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>Date</th>
            <th>Available Cleaner</th>
        </tr>
		@foreach ($calendarmaster as $key => $val)
		<tr>
			<td>{{ date('d/m/Y',strtotime($key)) }}</td>
			<td>
				<table>
				<tr>
				@foreach($val as $key01=>$data)
					<td>
						<a href="{{'/admin/user/'.$key01.'/edit'}}" style="color:#FFF;">{{ $users[$key01]['name']}}</a>
						<ul style="padding-left:inherit;">
						@foreach($data as $time)
							<li>{{$time['start_time'].'Hrs - '.$time['end_time'].'Hrs'}}</li>
						@endforeach
						@if(isset($bookingmaster[$key][$key01]))
						<li style="margin-top:10px; list-style:none;">Booking List</li>
						@foreach($bookingmaster[$key][$key01] as $time)
							<li>{{$time['from_time'].'Hrs - '.$time['to_time'].'Hrs'}}</li>
						@endforeach
						@endif
						</ul>
					</td>
				@endforeach
				</tr>
				</table>
			</td>
		</tr>
		@endforeach
    </table>


    <?php /*{!! $user->render() !!}*/?>

		</div>
	</div>
</div>
@endsection