@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Cleaner Details</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::model($user, ['method' => 'PATCH','route' => ['user.update', $user->id]]) !!}
    <div class="row border border-primary bg-white p-2">


        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Name : {{ $user['name'] }}</strong>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Email Id:</strong>
                {!! Form::text('email', null, array('placeholder' => 'Email Id','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>Phone Number</strong>
                {!! Form::text('phone_number', null, array('placeholder' => 'Phone Number','class' => 'form-control')) !!}
            </div>
        </div>
		<div class="col-xs-6 col-sm-6 col-md-6 d-nonew">
			<div class="form-group">
				<strong>Location</strong>
				<select class="form-control" name="location_id">
					@foreach(locationlist() as $val)
						<option value="{{ $val['id'] }}"  @if($val['id'] == $address[0]['location_id']){{ 'selected' }}@endif>{{ $val['name'] }}</option>
					@endforeach
				</select>
			</div>			
		</div>
        <div class="col-xs-6 col-sm-6 col-md-6 d-nonew">
            <div class="form-group">
                <strong>Address</strong>
                {!! Form::text('location_id', $address[0]['address_details'], array('placeholder' => 'Location','class' => 'form-control')) !!}
            </div>
        </div>
				




        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary float-left">Submit</button>
        </div>

    </div>
    {!! Form::close() !!}

	
	<div class="row border border-primary bg-white p-2">
	
    	<link type="text/css" rel="stylesheet" href="/calendar/calendamedia/layout.css" />

        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_g.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_green.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_traditional.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_transparent.css" />
        <link type="text/css" rel="stylesheet" href="/calendar/themes/calendar_white.css" />

	
	<!-- daypilot libraries -->
        <script src="/calendar/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>	
	
	
        <div class="main">

            <div style="float:left; width: 160px;">
                <div id="nav"></div>
            </div>
            <div style="margin-left: 160px;">

                <div class="space d-none">
                    Theme: <select id="theme">
                        <option value="calendar_default">Default</option>
                        <option value="calendar_white">White</option>
                        <option value="calendar_g">Google-Like</option>
                        <option value="calendar_green">Green</option>
                        <option value="calendar_traditional">Traditional</option>
                        <option value="calendar_transparent">Transparent</option>
                    </select>
                </div>

                <div id="dp"></div>
            </div>

            <script type="text/javascript">

                var nav = new DayPilot.Navigator("nav");
                nav.showMonths = 1;
                nav.skipMonths = 1;
                nav.selectMode = "week";
                nav.onTimeRangeSelected = function(args) {
                    dp.startDate = args.day;
                    dp.update();
                    loadEvents();
                };
                nav.init();

                var dp = new DayPilot.Calendar("dp");
                dp.viewType = "Week";
				dp.theme = "calendar_green";
                dp.eventDeleteHandling = "Update";

                dp.onEventDeleted = function(args) {
                    $.post("backend_delete.php",
                        {
                            id: args.e.id()
                        },
                        function() {
                            console.log("Deleted.");
                        });
                };

                dp.onEventMoved = function(args) {
                    $.post("backend_move.php",
                            {
                                id: args.e.id(),
                                newStart: args.newStart.toString(),
                                newEnd: args.newEnd.toString()
                            },
                            function() {
                                console.log("Moved.");
                            });
                };

                dp.onEventResized = function(args) {
                    $.post("backend_resize.php",
                            {
                                id: args.e.id(),
                                newStart: args.newStart.toString(),
                                newEnd: args.newEnd.toString()
                            },
                            function() {
                                console.log("Resized.");
                            });
                };

                // event creating
                dp.onTimeRangeSelected = function(args) {
					//alert(args.start);
                    //alert(args.start);
                    //var name = prompt("New event name:", "Available");
                    var name = "Available";
                    dp.clearSelection();
                    if (!name) return;
                    var e = new DayPilot.Event({
                        start: args.start,
                        end: args.end,
                        id: DayPilot.guid(),
                        resource: args.resource,
                        text: name
                    });
                    dp.events.add(e);

                    $.post("backend_create.php",
                            {
                                start: args.start.toString(),
                                end: args.end.toString(),
                                name: name
                            },
                            function() {
                                console.log("Created.");
                            });

                };

                dp.onEventClick = function(args) {
                    //alert("clicked: " + args.e.id());
                };

                dp.init();

                loadEvents();

                function loadEvents() {
                    dp.events.load("backend_events.php");
                }
    var e = new DayPilot.Event({
        start: new DayPilot.Date("2019-12-18T12:00:00"),
        end: new DayPilot.Date("2019-12-18T12:00:00").addHours(3),
        id: DayPilot.guid(),
        text: "Available"
    });
    dp.events.add(e);
            </script>

            <script type="text/javascript">
            $(document).ready(function() {
                $("#theme").change(function(e) {
                    dp.theme = this.value;
                    dp.update();
                });
            });
            </script>

        </div>
        <div class="clear">
        </div>


	
	
	
	</div>
	
		</div>
	</div>
</div>

@endsection