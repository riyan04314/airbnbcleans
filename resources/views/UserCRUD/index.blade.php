@extends('layouts.app')
 

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Customer List</h2>
            </div>
            <div class="float-right d-none">
                <a class="btn btn-success" href="{{ route('user.create') }}"> Create New User</a>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered table-striped table-dark">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Contact Number</th>
            <th>Address</th>
            <th width="280px">Action</th>
        </tr>
		@foreach ($users as $key => $user)
		<tr>
			<td>{{ ++$i }}</td>
			<td>{{ $user->name }}</td>
			<td>
				{{ $user->phone_number }}<br>
				<a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
			</td>
			<td>
				@if(isset($address[$user->id]))
				@foreach($address[$user->id] as $val)
					City : {{ $location[$val['location_id']]}}<br>
					Address : {{ $val['address_details']}}
				@endforeach
				@endif
			</td>
			<td>
				<a class="btn btn-sm btn-info"  onclick="getOrderdata({{$user->id}})"   >Order List</a>
				<?php /*<a class="btn btn-primary" href="#">Edit</a>{{ route('user.edit',$user->id) }}*/ ?>
				{!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->id],'style'=>'display:inline']) !!}
				{!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
				{!! Form::close() !!}
				
			</td>
		</tr>
		<tr style="display:none;" id="orderlist_{{$user->id}}" class="ord_data">
			<td colspan="5" style="text-align:center;">
				@if(isset($subscription[$user->id]))
					<table id="tbl_{{$user->id}}" class="table table-bordered table-striped table-dark tbl" style="font-size:smaller;" >
						<thead>
							<tr>
								<th>SL.</th>
								<th>Date</th>
								<th>Invoice Id</th>
								<th>Plan Name</th>
								<th>Received Token</th>
								<th>Used Token</th>
								<th>Remaining Token</th>
							</tr>
						</thead>
						<tbody>
							@php($i = 0)
							@foreach($subscription[$user->id] as $row)
								<tr>
									<td>{{$i++}}</td>
									<td>{{$row['created_at']}}</td>
									<td>{{$row['stripe_id']}}</td>
									<td>{{$row['stripe_plan']}}</td>
									<td>{{$row['quantity']}}</td>									
									<td>{{$row['used_quantity']}}</td>									
									<td>{{($row['quantity']-$row['used_quantity'])}}</td>									
								</tr>
							@endforeach							
						</tbody>
					</table>
				@else
					No subscription plan brought by this user.
				@endif	
			</td>
		</tr>
		@endforeach
    </table>


    <?php /*{!! $user->render() !!}*/?>

		</div>
	</div>
</div>

<style>
.tbl th{
	color:#000;
	background-color:#929da7
}
.tbl th, .tbl td{
	padding:1px;
}
</style>
		
<div id="schedulebookingmodal" class="modaltaskdecline" style="display:none; max-width: 600px;width: 600px; padding:15px;">
	<div class="modal-body" style="color:#000;">
		<h3>Order List</h3>
		<table>
			<tr><th>SL.</th><th>Date</th><th>Plan Name</th><th>Purchased Date</th><th>Received Token</th><th>Used Token</th><th>Remaining Token</th></tr>
		</table>
	</div>
</div>
<a href="#schedulebookingmodal" id="schedulebooking" style="display:none;" rel="modal:open">Open Modal</a>
<script>
$(document).ready(function () {  
	//alert(99);
	$('#schedulebooking').click();
	//alert(44);
});
getOrderdata = function(){
	$('.tbl').hide();
	$('.ord_data').hide();
	$('#orderlist_'+arguments[0]).show();
	$('#tbl_'+arguments[0]).show();
}

</script>
@endsection